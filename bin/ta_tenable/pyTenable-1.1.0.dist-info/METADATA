Metadata-Version: 2.1
Name: pyTenable
Version: 1.1.0
Summary: Python library to interface into Tenable's products and applications
Home-page: https://github.com/tenable/pytenable
Author: Tenable, Inc.
Author-email: smcgrath@tenable.com
License: MIT
Keywords: tenable tenable_io securitycenter containersecurity
Platform: UNKNOWN
Classifier: Development Status :: 4 - Beta
Classifier: Intended Audience :: Developers
Classifier: Topic :: Software Development
Classifier: Topic :: Software Development :: Libraries
Classifier: Topic :: Software Development :: Libraries :: Application Frameworks
Classifier: License :: OSI Approved :: MIT License
Classifier: Programming Language :: Python :: 2
Classifier: Programming Language :: Python :: 2.7
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.4
Classifier: Programming Language :: Python :: 3.5
Classifier: Programming Language :: Python :: 3.6
Classifier: Programming Language :: Python :: 3.7
Requires-Dist: requests (>=2.19)
Requires-Dist: python-dateutil (>=2.6)
Requires-Dist: semver (>=2.8.1)
Requires-Dist: ipaddress (>=1.0.22)
Provides-Extra: nessusreportv2
Requires-Dist: defusedxml (>=0.5.0) ; extra == 'nessusreportv2'
Provides-Extra: pwcertauth
Requires-Dist: requests-pkcs12 (>=1.3) ; extra == 'pwcertauth'
Provides-Extra: complete
Requires-Dist: defusedxml (>=0.5.0) ; extra == 'complete'
Requires-Dist: requests-pkcs12 (>=1.3) ; extra == 'complete'
Requires-Dist: docker (>=3.7.2) ; extra == 'complete'
Provides-Extra: docker
Requires-Dist: docker (>=3.7.2) ; extra == 'docker'

Welcome to pyTenable's documentation!
=====================================

.. image:: https://travis-ci.org/tenable/pyTenable.svg?branch=master
   :target: https://travis-ci.org/tenable/pyTenable
.. image:: https://img.shields.io/pypi/v/pytenable.svg
   :target: https://pypi.org/project/pyTenable/
.. image:: https://img.shields.io/pypi/pyversions/pyTenable.svg
   :target: https://pypi.org/project/pyTenable/
.. image:: https://img.shields.io/pypi/dm/pyTenable.svg
   :target: https://github.com/tenable/pytenable
.. image:: https://img.shields.io/github/license/tenable/pyTenable.svg
   :target: https://github.com/tenable/pytenable

pyTenable is intended to be a pythonic interface into the Tenable application 
APIs.  Further by providing a common interface and a common structure between 
all of the various applications, we can ease the transition from the vastly 
different APIs between some of the products.

- Issue Tracker: https://github.com/tenable/pyTenable/issues
- Github Repository: https://github.com/tenable/pyTenable

Installation
------------

To install the most recent published version to pypi, its simply a matter of 
installing via pip:

.. code-block:: bash

   pip install pytenable

If you're looking for bleeding-edge, then feel free to install directly from the 
github repository like so:

.. code-block:: bash

   pip install git+git://github.com/tenable/pytenable.git#egg=pytenable

Getting Started
---------------

Lets assume that we want to get the list of scans that have been run on our 
Tenable.io application.  Performing this action is as simple as the following:

.. code-block:: python

   from tenable.io import TenableIO
   tio = TenableIO('TIO_ACCESS_KEY', 'TIO_SECRET_KEY')
   for scan in tio.scans.list():
      print('{status}: {id}/{uuid} - {name}'.format(**scan))

Getting started with Tenable.sc is equally as easy:

.. code-block:: python

   from tenable.sc import TenableSC
   sc = TenableSC('SECURITYCENTER_NETWORK_ADDRESS')
   sc.login('SC_USERNAME', 'SC_PASSWORD')
   for vuln in sc.analysis.vulns():
      print('{ip}:{pluginID}:{pluginName}'.format(**vuln))

For more detailed information on whats available, please refer to the
`pyTenable Documentation <https://pytenable.readthedocs.io/en/latest/>`_

Logging
-------

Enabling logging for pyTenable is a simple matter of enabling debug logs through
the python logging package.  An easy example is detailed here:

.. code-block:: python

   import logging
   logging.basicConfig(level=logging.DEBUG)

License
-------

The project is licensed under the MIT license.


