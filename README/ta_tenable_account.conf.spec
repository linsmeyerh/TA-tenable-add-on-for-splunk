[<name>]
tenable_account_type = 
address = FQDN or IP of your server.
verify_ssl = Should we verify your SSL certificate?
access_key = Enter the access key for this account.
secret_key = Enter the secret key for this account.
username = Enter the username for this account.
password = Enter the password for this account.
certificate_path = Enter the certificate file path for this account.
key_file_path = Enter the key file path for this account.
key_password = Enter the key password for this account.
proxy_enabled = Boolean to enable/disable proxy.
proxy_type = Enter the type of proxy to use.
proxy_url = Enter the URL for proxy.
proxy_port = Enter the port for proxy.
proxy_username = Enter the username for proxy authentication.
proxy_password = Enter the password for proxy authentication.